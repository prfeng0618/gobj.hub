package ch5

import (
	"errors"
	"fmt"
	"reflect"
	"sync"
	"time"
	"unsafe"
)

func Division(a, b float64) (float64, error) {
	if b == 0 {
		return 0, errors.New("除数不能为0")
	}

	return a / b, nil
}

func SliceMain() {
	var a []int
	b := []int{}

	fmt.Printf("a: %#v\n", (*reflect.SliceHeader)(unsafe.Pointer(&a)))
	fmt.Printf("b: %#v\n", (*reflect.SliceHeader)(unsafe.Pointer(&b)))
	fmt.Printf("a size: %d\n", unsafe.Sizeof(a))
	fmt.Printf("b size: %d\n", unsafe.Sizeof(b))
}

func Slice2Main() {
	s := make([]int, 0, 100)
	s1 := s[:2:4]
	s2 := append(s1, 1, 2, 3, 4, 5, 6)

	fmt.Printf("s1: %p: %v\n", &s1[0], s1)
	fmt.Printf("s2: %p: %v\n", &s2[0], s2)

	fmt.Printf("s data %v\n", s[:10])
	fmt.Printf("s1 cap %d, s2 cap %d \n", cap(s1), cap(s2))
}

func MapMain() {
	//m := make(map[int]int)
	m := map[int]int{}

	for i := 0; i < 10; i++ {
		m[i] = i + 10
	}

	for k := range m {
		if k == 5 {
			m[100] = 1000
		}
		delete(m, k)
		fmt.Println(k, m)
	}
}
func MapRaceMain() {
	m := make(map[string]int)

	go func() {
		for {
			m["a"] += 1
			time.Sleep(time.Millisecond)
		}
	}()

	go func() {
		for {
			_ = m["b"]
			time.Sleep(time.Millisecond)
		}
	}()

	select {}
}

func MapRace2Main() {
	m := make(map[string]int)
	var lock sync.RWMutex

	go func() {
		for {
			lock.Lock()
			m["a"] += 1
			lock.Unlock()
			time.Sleep(time.Millisecond)
		}
	}()

	go func() {
		for {
			lock.RLock()
			_ = m["b"]
			lock.RUnlock()
			time.Sleep(time.Millisecond)
		}
	}()

	select {}
}
