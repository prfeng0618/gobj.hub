package ch4

import "fmt"

func SlipTestMain() {
	a := []int{10, 20, 30}
	sliptest(a...)

	fmt.Println(a)
}

func sliptest(a ...int) {
	for i := range a {
		a[i] += 100
	}
}

func ClosureTestMain() {
	for _, f := range closuretest2() {
		f()
	}
}

func closuretest() []func() {
	var s []func()

	for i := 0; i < 2; i++ {
		s = append(s, func() {
			println(&i, i)
		})
	}

	return s
}

func closuretest2() []func() {
	var s []func()

	for i := 0; i < 2; i++ {
		x := i
		s = append(s, func() {
			println(&x, x)
		})
	}

	return s
}

func ClosureTestMain2() {
	a, b := closuretest3(100)
	a()
	b()
}

func closuretest3(x int) (func(), func()) {
	return func() {
			println(x)
			x += 10
		}, func() {
			println(x)
		}
}
func DeferMain() {
	x, y := 1, 2

	defer func(a int) {
		println("defer x ,y=", a, y)
	}(x)

	x += 100
	y += 200
	println(x, y)
}

func DeferMain2() {
	println("test:", defertest())
}

func defertest() (z int) {
	defer func() {
		println("defer:", z)
		z += 100
	}()

	return 100
}
