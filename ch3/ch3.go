package ch3

import "fmt"

func RangTestMain() {
	date := [3]int{10, 20, 30}
	fmt.Printf("(%p):(%p):(%p)\n", &date[0], &date[1], &date[2])

	for i, x := range date {
		if i == 0 {
			date[0] += 100
			date[1] += 200
			date[2] += 300
		}

		fmt.Printf("x(%p): %d, data(%p): %d\n", &x, x, &date[i], date[i])
	}

	for i, x := range date[:] {
		if i == 0 {
			date[0] += 100
			date[1] += 200
			date[2] += 300
		}

		fmt.Printf("x(%p): %d, data(%p): %d\n", &x, x, &date[i], date[i])
	}
}
